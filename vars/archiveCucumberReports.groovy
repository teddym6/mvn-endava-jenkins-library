def call(){
     publishHTML target: [
            allowMissing         : false,
            alwaysLinkToLastBuild: false,
            keepAll              : true,
            reportDir            : 'results',
            reportFiles          : 'cucumber-reports.html',
            reportName           : 'Selenium spring boot training Report'
    ]
}